<?php 
/**
* Este clase contiene todas las funciones principales que
* se pueda usar para el funcionamiento del mainpage
*/

class MainFunciones 
{
    public function switchStatus($estado,$mhabilitado,$mdeshabilitado){
        
       return $efecto =($estado == 1) ? "class='btn btn-default'>$mhabilitado" :"class='btn btn-danger'>$mdeshabilitado"  ;
        
    }//fin funcion switch
    
    public function setMensajes($tipo,$title,$mensaje)
    {
      return $mensaje="<div class='alert alert-".$tipo." alert-dismissable'>
                                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <strong>".$title."</strong>".$mensaje."</div>";
    }//fin de la funcion mensaajes
    
    public function setProblems($tipo){
        return $clase = (is_null($tipo)) ? " class='danger'" : "";  
    }
    
    public function despadazaPostAjax($cadena /*String*/)
    {
        $parametros = array();
        $i=0;$y=0; /*variables que hacen el rastreo de valores y id de las widgets*/
        $cadena = str_replace('=','&',$cadena);/*trasforma la cadena obtenida con solo identificador*/
        $arreglo= explode("&", $cadena);
            while ($i < (count($arreglo))):
                $parametros[$y] = str_replace('+',' ',$arreglo[$i+1]);//conforma la cadena eliminando los + que genera el serializarable
                $i=$i+2;/*aumento de la variable para que solo obtenaga los valores*/$y++;
            endwhile;
        return $parametros;//envio de todos lo valores de un formulario
    }//esta funcion permite obtener  todos lo valores apartir de un form para esto debe estan encerrado en una etiqueta <form>
    
    
    public function existenvaloresVacios($valores/*Array*/)
    {
        $bandera = true;
        foreach ($valores as $value) 
        {
            if(empty($value))//evalua campo por campo
                {
                $bandera=false; //si existe almenos un campo vacio sera false
                }  
        }//validacion del valores vacions 
        return $bandera;
    }//funcion que valida si existen campos vacios
}//fin del clase

?>