<?php
require_once 'configuracion.php';

class CargaArchivos extends Config_Ini
{
public function getFotografia()
{
    /* Extensiones de archivo permitidas */ 
    $ext_permitidas = array("jpg","jpeg","gif","bmp","png");

    /*MIME Types permitidos*/
    $mime_permitidas = array("image/jpg","image/jpeg","image/gif","image/bmp","image/png");

    /* Directorio donde se almacenaran los archivos subidos */
    $directorio = self::_UPLOADIMG;

    /* Tamaño maximo que se aceptara (5mb) */
    $max_tamano = 5*(1024*1024);

    /* Sobreescribir el archivo si existe, si se deja en false genera un nombre nuevo */
    $sobreescribir = false;

    /* Comprueba si se recibio un archivo */
    if (isset($_FILES["archivo"]["name"])) {
	
	/* Variables principales del archivo subido contenidas en el array $_FILE */
	$file_name = $_FILES["archivo"]["name"];
	$file_temp = $_FILES["archivo"]["tmp_name"];
	$file_size = $_FILES["archivo"]["size"];
	$file_type = $_FILES["archivo"]["type"];
	$error = $_FILES["archivo"]["error"];
	
	/* Comprobacion de los errores automaticos (generados) */
	/* Si $error es cero significa que no hay ningun error */
	if ($error) {
		switch ($error) {
			case 1: die("ERROR: El tamaño es mayor al definido por la configuracion de PHP."); break;
			case 2: die("ERROR: El tamaño es mayor al definido en el formulario HTML."); break;
            case 3: die("ERROR: El archivo solo fue parcialmente subido."); break;
            case 4: die("ERROR: Ningun archivo fue subido."); break;
			case 6: die("ERROR: No existe un directorio para archivos temporales."); break;
			case 7: die("ERROR: Fallo la escritura en disco del archivo."); break;
			case 8: die("ERROR: Alguna extension de PHP interrumpio la subida del archivo.");
		}
	}//fin if errrores
	
	/* Comprueba si el tamaño del archivo no supera el tamaño maximo definido */
	if ($file_size>$max_tamano) {
		die("ERROR: Tamaño maximo superado.");
	}
    
	/* Obtiene la extension del archivo */
	if (!(strpos($file_name,".")===false)) {
		$file_ext = strtolower(end(explode(".",$file_name)));
	}
	
	/* Comprueba si la extension del archivo esta permitida */
	if (isset($file_ext)) {
		if (!in_array($file_ext,$ext_permitidas)) {
			die("ERROR: El Formato del archivo no esta permitido.");
            //exit();
		}
	}
	
	/* Comprueba el mime-type que nos da el navegador (cliente: inseguro) */
	if (!in_array($file_type,$mime_permitidas)) {
		die("ERROR: El formato del archivo no esta permitido.");
        //exit();
	}
	
	/* Comprueba el mime-type sacado por PHP (lado del servidor: seguro)
	$file_mime_type = reset(explode(";",finfo_file(finfo_open(FILEINFO_MIME),$file_temp)));
	if (!in_array($file_mime_type,$mime_permitidas)) {
		die("ERROR: El Formato del archivo no esta permitido.");
	}*/
    
	/* Comprueba si el archivo existe en la ubicacion donde lo vamos a copiar */
	if (file_exists($directorio.$file_name)) {
		/* Si la variable $sobreescribir es false, se le cambia el nombre al archivo */
		/* En caso contrario, simplemente se deja que se sobreescriba */
		if (!$sobreescribir) {
			$i = 1;
			while ($i) {
				if (!file_exists($directorio.$i."_".$file_name)) {
					$file_name = $i."_".$file_name;
					$i = 0;
				} else {
					$i++;
				}
			}
		}
	}//sobreescribir archivos	
	/* Mueve el archivo de su ruta temporal a la que nosotros le queramos asignar */
	if (move_uploaded_file($file_temp,$directorio.$file_name)) {
		//echo "El archivo se subio exitosamente al servidor.";
	} else {
		echo "ERROR: El archivo no se logro subir con exito.";
	}
	
}
return self::_UPLOADIMG;
}

}//fin de la clase
?>